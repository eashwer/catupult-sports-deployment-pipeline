#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/catupult-sports-deployment-pipeline/

# clone the repo again
#git clone https://gitlab.com/eashwer101/catapult-sports.git
git clone https://gitlab.com/eashwer/catupult-sports-deployment-pipeline.git

whoami
docker -v
#docker kill $(docker ps -q)
#docker rm $(docker ps -a -q)
#docker rmi $(docker images -q)
ls
cd catupult-sports-deployment-pipeline/

make update
pwd
#docker stop flaskawscli
#docker stop nginx

#docker rm flaskawscli
#docker rm nginx
#docker network rm myflasknginx-net	
docker images
docker ps -a
make run

#if deploying second time enable the following 
#docker stop flaskawscli
#docker stop nginx

#docker rm flaskawscli
#docker rm nginx
#docker network rm myflasknginx-net	






